'use strict';

/*
 * exogpu.js: project root
 *
 * (C) Copyright ${YEAR}
 * Author: Julien Sarriot <julien.sarriot@epitech.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 3
 * of the License.
 */

const customtweak = require('./power/customtweak');

customtweak();
