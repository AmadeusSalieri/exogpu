'use strict';

/*
 *  exogpu
 *  Copyright (C) 2018  Samuel Radat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const {readFileSync, readdirSync} = require('fs');
const logger = require('./utils/logger');

/**
 * This class represents the chip statistics object
 */
class ChipStat {
  /**
   * constructor
   */
  constructor() {

    this.chipsPath = '/sys/class/hwmon';
    this.chipPaths = readdirSync(this.chipsPath);

    this.chipPath = `/sys/class/hwmon/hwmon${this.findChip()}`;
    this.chipSysFiles = readdirSync(this.chipPath);

    this.chipNb = this.chipPaths.length;

    this.name = readFileSync(`${this.chipPath}/name`);

    this.temperature = [];

    this.update();

    /*logger.info(`${this.chipNb} chips detected`);
    logger.debug(this.chipPaths);

    logger.info(`Chip name: ${this.name}`);
    logger.info(`Temperature: ${this.temperature}°C`);
    logger.debug(this.chipSysFiles);*/
  }

  /**
   * Retrieves temperature of card
   */
  retrieveTemperature() {
    try {
      this.temperature = [];
      readdirSync(this.chipPath).forEach((infoFile) => {
        if (infoFile.startsWith('temp') && infoFile.endsWith('_input')) {
          this.temperature.push(parseInt(
            readFileSync(`${this.chipPath}/${infoFile}`)) / 1000
          );
        }
      });
      /*if (this.temperature.length === 0) {
        throw new Error('Failed to retrieve temperature');
      }*/
    } catch (e) {
      this.temperature = ['Unknown'];
      logger.warn(e);
    }
  }

  /**
   * Retrieves fans status
   */
  retrieveFans() {
    try {
      this.fan = [];
      readdirSync(this.chipPath).forEach((infoFile) => {
        if (infoFile.startsWith('fan') && infoFile.endsWith('_input')) {
          this.fan.push(`${readFileSync(`${this.chipPath}/${infoFile}`)}RPM`);
        }
      });
      /*if (this.fan.length === 0) {
        throw new Error('No fans detected');
      }*/
    } catch (e) {
      this.fan = ['Unknown'];
      logger.warn(e);
    }
  }

  /**
   * Retrieve current chip informations
   */
  retrieveAll() {
    this.retrieveTemperature();
    this.retrieveFans();
  }

  /**
   * Update temperature
   */
  updateTemp() {
  }

  /**
   * Update fan
   */
  updateFan() {
  }

  /**
   * Update current chip status
   */
  update() {
  }

  /**
   * Prints current chip status
   */
  print() {
    logger.info(`Temperatures : ${this.temperature}`);
    logger.info(`Fans : ${this.fan}`);
  }

  /**
   * Finds the chip index number
   * @returns {number}
   */
  findChip() {
    return 0;
  }
}

module.exports = ChipStat;
