const ncurse = require('blessed');
const contrib = require('blessed-contrib');

const GpuStat = require('../chipStat');
const DRM = require('../GPUList');

const screen = ncurse.screen({
  smartCSR: true,
});

screen.title = 'ExoGPU - Unleash your GPU';

screen.key(['escape', 'q', 'C-c'], () => process.exit(0));

const grid = new contrib.grid({
  rows: 2,
  cols: 2,
  screen: screen,
});

class GraphStats {
  constructor() {
    this._stat = new GpuStat();
    this._refreshData();

    this.temps = {
      graph: this._createTempGraphLine(),
      values: this._stat.temperature.map((temp, idx) => {
        const date = new Date();

        return {
          title: 'Chip n°' + idx,
          x: [this.lastUpdate],
          y: [temp],
          style: {
            line: this.getRandomColor(),
          },
        };
      }),
    };
  }

  getRandomColor() {
    return [Math.random() * 255, Math.random() * 255, Math.random() * 255];
  }

  _leftPad(number, targetLength) {
    let output = number + '';

    while (output.length < targetLength) {
      output = '0' + output;
    }
    return output;
  }

  _refreshData() {
    this._stat.retrieveAll();

    const date = new Date();
    this.lastUpdate = `${
      this._leftPad(date.getMinutes(), 2)
    }:${
      this._leftPad(date.getSeconds(), 2)
    }`;
  }

  updateTempGraphLine() {
    this._refreshData();
    this._stat.temperature.forEach((temp, idx) => {
      const chip = this.temps.values[idx];

      if (chip.x.length >= 5) {
        chip.x.shift();
      }
      chip.x.push(this.lastUpdate);

      if (chip.y.length >= 5) {
        chip.y.shift();
      }
      chip.y.push(temp);
    });
    this.temps.graph.setData(this.temps.values);
  }

  _createTempGraphLine() {
    return grid.set(0, 0, 2, 1, contrib.line, {
      style: {
        line: this.getRandomColor(),
        text: this.getRandomColor(),
        baseline: this.getRandomColor(),
      },
      showLegend: true,
      wholeNumbersOnly: false,
      label: 'GPUs temperature (°C)',
    });
  }
}

const graphStat = new GraphStats();

const table = grid.set(0, 1, 1, 1, contrib.table, {
  keys: false,
  interactive: false,
  label: 'Graphics Cards',
  columnWidth: [6, 20, 30],
});

table.setData({
  headers: ['Hwmon', 'Vendor', 'Device'],
  data: (() => {
    const array = [];

    DRM.cards.forEach((device, key) => {
      array.push([key, device._vendor.name, device._name]);
    });
    return array;
  })(),
});

setInterval(() => {
  graphStat.updateTempGraphLine();
  screen.render();
}, 2000);
