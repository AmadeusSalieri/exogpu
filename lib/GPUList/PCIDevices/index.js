'use strict';

/*
 * exogpu
 * Copyright (C) 2018  Julien Sarriot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const {EOL} = require('os');
const {readFileSync, existsSync} = require('fs');

const Vendor = require('./components/vendor');

/**
 * PCIDevices
 */
class PCIDevices {
  /**
   * @param {Array.<string>} pciids
   */
  constructor(pciids) {
    /**
     * Vendors list
     * @type {Array.<Vendor>}
     * @private
     */
    this._vendors = [];

    pciids.forEach((pciid) => {
      if (pciid.startsWith('#') || pciid.startsWith('C')) return;
      const [vendor, device, subdevice] = pciid.split('\t');

      if (vendor.length > 0) {
        this._parseVendor(vendor);
      } else if (device && !subdevice) {
        this._parseDevice(device);
      } else if (subdevice) {
        this._parseSubDevice(subdevice);
      }
    });
  }

  /**
   * Find vendor by id
   * @param {string} id - vendor id in hex format
   * @return {Vendor}
   */
  findVendorById(id) {
    return this._vendors.find((vendor) => vendor.id === id);
  }

  /**
   * Parse vendor line
   * @param {string} pciid
   * @private
   */
  _parseVendor(pciid) {
    const regex = /([a-fA-F0-9]{4})(?:\s+)(.*)/;
    const [, id, name] = (pciid.match(regex) || []);

    if (id && name) {
      const vendor = this.findVendorById(id) || new Vendor(id);

      vendor.name = name;
      this._vendors.push(vendor);
    }
  }

  /**
   * Parse subdevice line
   * @param {string} pciid
   * @private
   */
  _parseSubDevice(pciid) {
    const regex = /([a-fA-F0-9]{4})(?:\s+)([a-fA-F0-9]{4})(?:\s+)(.*)/;
    const [, vendorId, id, name] = (pciid.match(regex) || []);

    if (vendorId && id && name) {
      let vendor = this.findVendorById(vendorId);

      if (vendor === undefined) {
        this._vendors.unshift(vendor = new Vendor(vendorId));
      }
      this.lastVendor.lastDevice
        .addSubdevice(id, name, vendor);
    }
  }

  /**
   * Parse device line
   * @param {string} pciid
   * @private
   */
  _parseDevice(pciid) {
    const regex = /([a-fA-F0-9]{4})(?:\s+)(.*)/;
    const [, id, name] = (pciid.match(regex) || []);

    if (id && name) {
      this.lastVendor.addDevice(id, name);
    }
  }

  /**
   * Get PCI last vendor
   * @return {Vendor}
   */
  get lastVendor() {
    const lastIndex = this._vendors.length - 1;

    if (lastIndex < 0) {
      throw new Error('PCI device list doesn\'t have vendor');
    }
    return this._vendors[this._vendors.length - 1];
  }
}

/**
 * Retrieve PCI devices list from file
 *
 * To update pci devices list.
 * bash command: update-pciids
 *
 * @type {Array.<string>}
 */
const pciids = (() => {
  const paths = ['/usr/share/misc/pci.ids', '/usr/share/hwdata/pci.ids'];
  const path = paths.find((path) => existsSync(path));

  return readFileSync(path, 'utf-8').split(EOL);
})();

module.exports = new PCIDevices(pciids);
