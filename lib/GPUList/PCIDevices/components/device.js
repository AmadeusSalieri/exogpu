'use strict';

/*
 * exogpu
 * Copyright (C) 2018  Julien Sarriot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Create a new PCI Device
 * @class
 */
class Device {
  /**
   * Initialize members variables
   * @param {string} id - device id in hex format
   * @param {string} name - device name
   * @param {Vendor} vendor - vendor instance
   */
  constructor(id, name, vendor) {
    this._name = name;
    this._vendor = vendor;
    this.id = id;

    /**
     * device subdevices list
     * @type {Array.<Device>}
     * @private
     */
    this._subdevices = [];
  }

  /**
   * Add a new subdevice
   * @param {string} id - subdevice id in hex format
   * @param {string} name - subdevice name
   * @param {Vendor} vendor - subdevice vendor
   * @return {number}
   */
  addSubdevice(id, name, vendor) {
    return this._subdevices.push(new Device(id, name, vendor));
  }

  /**
   * Find subdevice by id
   * @param {string} id - subdevice id in hex format
   * @return {Device}
   */
  findSubDeviceById(id) {
    return this._subdevices.find((subdevice) => subdevice.id === id);
  }

  /**
   * Set device id
   * @param {string} id - device id in hex format
   */
  set id(id) {
    if (!/^[a-fA-F0-9]+$/.test(id)) {
      throw new Error('Invalid device id');
    }
    this._id = id.toLowerCase();
  }

  /**
   * Get device id
   * @return {string}
   */
  get id() {
    return this._id;
  }

  /**
   * Get device name
   * @return {string}
   */
  get name() {
    return `${this._vendor.name} ${this._name}`;
  }
}

module.exports = Device;
