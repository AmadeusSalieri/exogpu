'use strict';

/*
 * index.js: project root
 *
 * (C) Copyright ${YEAR}
 * Author: Julien Sarriot <julien.sarriot@epitech.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 3
 * of the License.
 */

require('./lib/utils/env');
const {app} = require('electron');
let Window = null;

let createWindow = () => {
  Window = require('./lib/gui/gui');
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (Window === null) {
    Window = require('./lib/gui/gui');
  }
});
