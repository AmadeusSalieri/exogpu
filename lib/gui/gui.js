/*
 *  exogpu
 *  Copyright (C) 2018  Samuel Radat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const {BrowserWindow} = require('electron');
const logger = require('../utils/logger');

class MainUI extends BrowserWindow {

  constructor() {

    super({width: 1280, height: 720});

    this.loadFile('./lib/gui/ui/index.html');

    return this;
  }

  initEvents() {
    this.on('closed', () => {
      logger.info('Window closed');
    });
  }

}

module.exports = new MainUI();
