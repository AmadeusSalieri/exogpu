'use strict';

/*
 * exogpu
 * Copyright (C) 2018  Julien Sarriot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const {readFileSync, readdirSync} = require('fs');
const {join} = require('path');

const PCIDevices = require('./PCIDevices');

/**
 * Direct Rendering Manager
 * @class
 */
class DirectRenderingManager {
  /**
   * Initialize cards list
   */
  constructor() {
    this._path = '/sys/class/drm';

    const cards = this._getAllCards()
      .map((card) => [card, this._getCard(card)]);

    this._cards = new Map(cards);
  }

  /**
   * Get card from PCI devices
   * @param {string} card
   * @return {Device}
   * @private
   */
  _getCard(card) {
    const references = ['vendor', 'device', 'subsystem_device'];
    const [vendorId, deviceId, subdeviceId] = references
      .map((ref) => this._getCardDesc(card, ref));

    return PCIDevices
      .findVendorById(vendorId)
      .findDeviceById(deviceId, subdeviceId);
  }

  /**
   * Get card information
   * @param {string} card
   * @param {string} info
   * @return {string}
   * @private
   */
  _getCardDesc(card, info) {
    const cardPath = join(this._path, card, 'device', info);

    return this._formatId(readFileSync(cardPath, 'utf-8'));
  }

  /**
   * Format specified id
   * @param {string} id - hex format
   * @return {string}
   * @private
   */
  _formatId(id) {
    if (id.startsWith('0x')) {
      return id.substr(2, id.length - 3);
    }
    return id.substr(0, id.length - 1);
  }

  /**
   * Retrieve all cards
   * @return {Array.<string>}
   * @private
   */
  _getAllCards() {
    const regex = /(?:card)(\d+)$/;

    return readdirSync(this._path)
      .filter((card) => regex.test(card));
  }

  /**
   * Get cards
   * @return {Map<string, Device>}
   */
  get cards() {
    return this._cards;
  }
}

module.exports = new DirectRenderingManager();
