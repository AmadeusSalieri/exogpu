# exogpu

GPU Linux Ecosystem. Tested on Ubuntu 18.04

### TODO:
Check GPU frequencies: /sys/class/drm/card0/device/drm/card0/gt_*
Retrieve HWMON index: /sys/class/drm/card0/device/index
Check: https://github.com/kobalicek/amdtweak

### Documentation

https://innovationepitech.gitbook.io/exogpu/

### Git flow pattern
 * https://git-flow.readthedocs.io/fr/latest/_images/gitflow.png
