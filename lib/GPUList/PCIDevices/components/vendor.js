/*
 * exogpu
 * Copyright (C) 2018  Julien Sarriot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

/*
 * exogpu
 * Copyright (C) 2018  Julien Sarriot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Device = require('./device');

/**
 * Create a new PCI vendor
 * @class
 */
class Vendor {
  /**
   * Initialize members variables
   * @param {string} id - vendor id in hex format
   * @param {string=} name - vendor name
   */
  constructor(id, name) {
    this.name = name || `unknown#${id}`;
    this.id = id;

    /**
     * Vendor devices list
     * @type {Array.<Device>}
     * @private
     */
    this._devices = [];
  }

  /**
   * Add a new device
   * @param {string} id - device id in hex format
   * @param {string} name - device name
   * @return {number}
   */
  addDevice(id, name) {
    return this._devices.push(new Device(id, name, this));
  }

  /**
   * Find device by id
   * @param {string} id - device id in hex format
   * @param {string=} subid - subdevice id in hex format
   * @return {Device}
   */
  findDeviceById(id, subid) {
    const device = this._devices.find((device) => device.id === id);

    if (device && subid !== undefined) {
      return device.findSubDeviceById(subid) || device;
    }
    return device;
  }

  /**
   * Get vendor last device
   * @return {Device}
   */
  get lastDevice() {
    const lastIndex = this._devices.length - 1;

    if (lastIndex < 0) {
      throw new Error('This vendor doesn\'t have device');
    }
    return this._devices[lastIndex];
  }

  /**
   * Set vendor id
   * @param {string} id - vendor id in hex format
   * @private
   */
  set id(id) {
    if (!/^[a-fA-F0-9]+$/.test(id)) {
      throw new Error('Invalid vendor id');
    }
    this._id = id.toLowerCase();
  }

  /**
   * Get vendor id
   * @return {string}
   */
  get id() {
    return this._id;
  }
}

module.exports = Vendor;
